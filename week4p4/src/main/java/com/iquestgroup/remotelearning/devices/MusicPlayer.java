package com.iquestgroup.remotelearning.devices;

import java.util.List;

import Interfaces.DeviceImpl;

public class MusicPlayer implements DeviceImpl {

	private Playlist playlist;

	public MusicPlayer(List<Song> songs) {
		this.playlist = new Playlist(songs);
	}

	@Override
	public void turnOn() {
		System.out.println("Music player turn on");
	}

	@Override
	public void turnOff() {
		System.out.println("Music player turn off");
	}

	@Override
	public void play() {
		System.out.println("Music player's playlist plays");
		playlist.playPlaylist();
	}

	@Override
	public void showSongs() {
		System.out.println("Music player's playlist: ");
		if (null != playlist.getPlaylist()) {
			playlist.showPlaylist();
		}else {
			System.out.println("No more songs");
		}
	}
}
