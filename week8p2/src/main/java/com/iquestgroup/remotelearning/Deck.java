package com.iquestgroup.remotelearning;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck {
	
	private List<Card> deck;

	public Deck() {
		deck = new ArrayList<Card>();
		fillDeck();
	}
	
	private void fillDeck() {
		for(Number number : Number.values()) {
			if(number.getValuOfCard() == 11) continue;
			for(Suit suit: Suit.values()) {
				deck.add(new Card(number, suit));
			}
		}
	}
	
	public void shuffle() {
		Collections.shuffle(deck);
	}
	
	public void printDeck() {
		for(Card card : deck) {
			System.out.println(card.toString());
		}
	}

	public List<Card> getDeck() {
		return deck;
	}
	
}
