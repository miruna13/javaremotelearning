package com.iquestgroup.remotelearning;

import java.util.List;

import com.iquestgroup.remotelearning.devices.Song;

import Interfaces.MusicianImpl;

public class Band {
	
	private String nameBand;
	private List<MusicianImpl> band;
	private List<Song> songs;

	public Band(List<MusicianImpl> band, List<Song> songs, String nameBand) {
		this.band = band;
		this.songs = songs;
		this.nameBand = nameBand;
		
		for(int i = 0; i < songs.size(); i++) {
			songs.get(i).setBand(nameBand);
		}
		
	}
	
	private void play() {
		for(MusicianImpl auto: band) {
			auto.playInstrument();
		}
	}
	
	private void compose(String name) {
		Song newSong = new Song(name);
		newSong.setBand(nameBand);
		songs.add(newSong);
	}
	
	public void registerSong(String name) {
		play();
		compose(name);
	}
	
	public void showSongs() {
		for(int i = 0; i < songs.size(); i++) {
			System.out.println(songs.get(i).getName());
		}
	}
	
}
