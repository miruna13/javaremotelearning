package com.iquestgroup.remotelearning;

public class Registrar {

	private void add(Registry registry, Domain newDomain) {
		registry.add(newDomain);
	}

	public boolean check(Registry registry, Domain newDomain) {
		for (Domain auxDomain : registry.getDomainsList()) {
			if (auxDomain.getName() == newDomain.getName()) {
				return false;
			}
		}
		add(registry, newDomain);
		System.out.println(registry.getDomainsList().toString());
		return true;
	}

	private void remove(Domain newDomain) {

	}


}
