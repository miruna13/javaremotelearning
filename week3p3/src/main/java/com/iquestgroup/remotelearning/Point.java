package com.iquestgroup.remotelearning;

public class Point extends Shape {
	
	private int X;
	private int Y;
	
	public Point(int x, int y) {
		X = x;
		Y = y;
	}

	@Override
	void draw() {
		System.out.println("Point draw");
	}

}
