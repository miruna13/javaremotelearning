package com.iquestgroup.remotelearning;

public enum TrainType {
	ELECTRIC(1), HIGH_SPEED(2), INTER_CITY(3);
	
	private int value;
	
	TrainType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

}
