package com.iquestgroup.remotelearning;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;


public class AppTest 
{
	TrainManager trainManager  = new TrainManager();
	
    @Test
    public void checkIfEqualObjectsAreStoredJustOnce() {
    	Train train1 = new Train(1, 2, 10);
    	Train train2 = new Train(1, 2, 10);
    	
    	trainManager.addTrain(train1);
    	trainManager.addTrain(train2);
    	
    	assertTrue(trainManager.getTrainsSet().size() == 1);
    }
}
