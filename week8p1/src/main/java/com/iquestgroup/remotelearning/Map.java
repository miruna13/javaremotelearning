package com.iquestgroup.remotelearning;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Map {

	private static List<Country> listOfCountries;
	
	public Map(ArrayList<Country> listOfCountries) {
		this.listOfCountries = listOfCountries;
	}
	
	public void sort() {
		Collections.sort(listOfCountries);
	}
	
	public void sortCapital() {
		Collections.sort(listOfCountries, new Comparator<Country>() {
		    public int compare(Country s1, Country s2) {
		        return s1.getCapital().compareTo(s2.getCapital());
		    }
		});
	}

	public void print() {
		System.out.println(listOfCountries.toString());
	}

	public int findBinarySearch(String capitalName) {

		sortCapital();
		int low = 0;
		int high = listOfCountries.size() - 1;
		int middle;

		while (low <= high) {
			middle = (low + high) / 2;
			int res = listOfCountries.get(middle).getCapital().compareTo(capitalName);

			if (res < 0) {
				low = middle + 1;
			} else if (res > 0) {
				high = middle - 1;
			} else {
				return middle;
			}
		}
		return -1;
	}

	public static List<Country> getMap() {
		return listOfCountries;
	}

}
