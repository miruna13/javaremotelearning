package com.iquestgroup.remotelearning.devices;

import java.util.List;

import Interfaces.DeviceImpl;

public class CDPlayer implements DeviceImpl {
	
	private Playlist playlist;
	
	public CDPlayer(List<Song> songs) {
		this.playlist = new Playlist(songs);
	}
	
	@Override
	public void turnOn() {
		System.out.println("CD player turn on");
	}

	@Override
	public void turnOff() {
		System.out.println("CD player turn off");
	}

	@Override
	public void play() {
		System.out.println("CD player's playlist plays");
		playlist.playPlaylist();
	}

	@Override
	public void showSongs() {
		System.out.println("CD player's playlist");
		if (playlist.getPlaylist().size() != 0) {
			playlist.showPlaylist();
		}else {
			System.out.println("No more songs");
		}
	}
	
}
