package com.iquestgroup.remotelearning;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class ProxyManager {

	public void a(Object o, String methodName) {
		Class klass = o.getClass();

		// iterate over its methods
		for (Method method : klass.getMethods()) {

			// verify if the method is the wanted one
			if (method.getName().equals(methodName)) {

				// yes, it is
				// so, iterate over its annotations
				for (Annotation annotation : method.getAnnotations()) {

					// verify if it is a LogMethodCall annotation
					if (annotation instanceof Logged) {

						// yes, it is
						// so, cast it
						Logged logged = (Logged) annotation;

						// verify the log level
						System.out.println("performing info log for \"" + method.getName() + "\" method");

					}
				}
			}
		}
	}

}
