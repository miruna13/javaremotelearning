package com.iquestgroup.remotelearning;

import java.util.ArrayList;
import java.util.List;

public class Paragraph {
	
	private List<Sentence> sentences;

	public Paragraph() {
		this.sentences = new ArrayList<Sentence>(20);
		for(int i = 0; i < 20; i++) {
			sentences.add(new Sentence());
		}
	}
	
	public void printParagraph() {
		for(Sentence newSentence : sentences) {
			newSentence.createSentence();
			System.out.print(newSentence.getSentence());
		}
		System.out.println("");
	}
	
}
