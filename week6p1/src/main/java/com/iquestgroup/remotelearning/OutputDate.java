package com.iquestgroup.remotelearning;

public class OutputDate {
	private Date date;
	private String finalDate;

	public OutputDate(Date date) {
		this.date = date;
		this.finalDate = "";
	}

	public void printDate() {
		setMonth();
		setDay();
		System.out.println(finalDate + "; Year: " + date.getYear());
	}

	private void setMonth() {
		for (Month month : Month.values()) {
			if (month.getValue() == date.getMonth()) {
				finalDate += "Month : " + month;
			}
		}
	}

	private void setDay() {
		for (Week week : Week.values()) {
			if (week.getValue() == date.getDay()) {
				finalDate += "; Day : " + week;
			}
		}
	}

}
