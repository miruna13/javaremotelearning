package com.iquestgroup.remotelearning;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws ClassNotFoundException
    {
    	ClassLoader classLoader = MyClass.class.getClassLoader();

        try {
            Class aClass = classLoader.loadClass("com.iquestgroup.remotelearning.MyClass");
            System.out.println("aClass.getName() = " + aClass.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        
        ClassLoader parentClassLoader = MyClass.class.getClassLoader();
        MyClass classLoader2 = new MyClass(parentClassLoader);

    }
}
