package com.iquestgroup.remotelearning;

import java.util.ArrayList;
import java.util.Collections;

public class App 
{
    public static void main( String[] args )
    {
    	ArrayList<Country> listForBinarySearch = new ArrayList<Country>();
		
		listForBinarySearch.add(new Country("A", "Bucuresti"));
		listForBinarySearch.add(new Country("B", "Amsterdam"));
		listForBinarySearch.add(new Country("D", "Atena"));
		listForBinarySearch.add(new Country("E", "Berlin"));
		listForBinarySearch.add(new Country("Franta", "Paris"));
		listForBinarySearch.add(new Country("C", "Budapesta"));
		listForBinarySearch.add(new Country("F", "Wrevan"));       
		
		Map map = new Map(listForBinarySearch);
		System.out.println(map.findBinarySearch("Paris"));
		System.out.println(map.getMap().toString());
        
    }
}
