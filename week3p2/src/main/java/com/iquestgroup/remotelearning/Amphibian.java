package com.iquestgroup.remotelearning;

public class Amphibian {
	
	public Amphibian() {
		System.out.println("Amphibian created");
	}
	
	public void speak() {
		System.out.println("Speak");
	}
	
	public void run() {
		System.out.println("Run");
	}
	
	public void eat() {
		System.out.println("Eat");
	}
	
	public void jump() {
		System.out.println("Jump");
	}
	
}
