package com.iquestgroup.remotelearning;

public class BaseClass {
	
	public int var = function(1);
	public static int staticVar = function(10);
	
	public BaseClass() {
		System.out.println("BaseClass Constructor");
	}
	
	{
		System.out.println("BaseClass Field");
	}
	
	static {
		System.out.println("BaseClass Static block");
	}
	
	private static int function(int number) {
		System.out.println("BaseClass Number: " + number);
		return number;
	}
	
}
