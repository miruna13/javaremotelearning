package com.iquestgroup.remotelearning;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class ProxyFactory implements Internet{
	
	private Internet internet = (Internet) new RealInternet(); 
    private static List<String> bannedSites;
    
    static
    { 
        bannedSites = new ArrayList<String>(); 
        bannedSites.add("site1"); 
        bannedSites.add("site2"); 
        bannedSites.add("site3"); 
        bannedSites.add("site4"); 
    }
	
    @Logged
	@Override
	public void connectTo(String serverhost) {
		if(bannedSites.contains(serverhost.toLowerCase())) 
        { 
            System.out.println("Access Denied"); 
        } 
          
        internet.connectTo(serverhost);
	}
	
}
