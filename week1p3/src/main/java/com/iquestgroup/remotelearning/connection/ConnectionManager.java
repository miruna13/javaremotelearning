package com.iquestgroup.remotelearning.connection;

import java.util.ArrayList;
import java.util.List;

public class ConnectionManager {

	private List<Connection> connectionList = new ArrayList<Connection>();
	private int noOfConnections = 5;
	private int currentPositionOfConnections = 0;

	public ConnectionManager() {
		
		for (int i = 0; i < noOfConnections; i++) {
			connectionList.add(new Connection());
		}
	}

	public Connection request() {
		if (currentPositionOfConnections < noOfConnections) {
			currentPositionOfConnections++;
			System.out.println("Connection returned");
			if(currentPositionOfConnections != 5)
			return connectionList.get(currentPositionOfConnections);
		}
		
		System.out.println("No more connection available");
		return null;
	}

}
