package com.iquestgroup.remotelearning;

public class App 
{
    public static void main( String[] args )
    {
        Domain domain1 = new Domain("domain1");
        Domain domain2 = new Domain("domain2");
        Domain domain3 = new Domain("domain1");
        Domain domain4 = new Domain("domain3");

        Registry registry = new Registry();
        
        Customer customer1 = new Customer();
        Customer customer2 = new Customer();
        
        customer1.buy(domain1, registry);
        customer1.buy(domain2, registry);
        customer1.buy(domain3, registry);
        customer2.buy(domain4, registry);
        
        customer2.buy(domain2, registry);
        
    }
}
