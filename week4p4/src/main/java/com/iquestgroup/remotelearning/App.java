package com.iquestgroup.remotelearning;

import java.util.ArrayList;
import java.util.List;

import com.iquestgroup.remotelearning.devices.CDPlayer;
import com.iquestgroup.remotelearning.devices.Ipod;
import com.iquestgroup.remotelearning.devices.MusicPlayer;
import com.iquestgroup.remotelearning.devices.Song;

import Interfaces.MusicianImpl;

public class App 
{
    public static void main( String[] args )
    {
        List<MusicianImpl> members = new ArrayList<MusicianImpl>();
        LeadGuitarist leadGuitarist = new LeadGuitarist();
        BassGuitarist bassGuitarist = new BassGuitarist();
        RhythmGuitarist rhythmGuitarist = new RhythmGuitarist();
        Drummer drummer = new Drummer();
        
        members.add(leadGuitarist);
        members.add(bassGuitarist);
        members.add(rhythmGuitarist);
        members.add(drummer);
        
        Song song1 = new Song("Song1");
        Song song2 = new Song("Song2");
        Song song3 = new Song("Song3");
        List<Song> album = new ArrayList<Song>();
        album.add(song1);
        album.add(song2);
        album.add(song3);
        
        List<Song> album2 = new ArrayList<Song>();
        album2.add(song1);
        album2.add(song2);
        
        Band band = new Band(members, album, "Band1");
        band.registerSong("Song4");
        
        Ipod ipod = new Ipod(album);
        MusicPlayer musicPlayer = new MusicPlayer(null);
        CDPlayer cdPlayer = new CDPlayer(album2);
        
        ipod.showSongs();
        musicPlayer.showSongs();
        cdPlayer.showSongs();
        
        ipod.play();
        
    }
}
