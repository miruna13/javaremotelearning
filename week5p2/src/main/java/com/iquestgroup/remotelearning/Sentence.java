package com.iquestgroup.remotelearning;

import java.util.ArrayList;
import java.util.List;

public class Sentence implements Titlelizer {

	private List<String> words = new ArrayList<String>();
	private String[] exceptions = { "the", "on", "to", "in", "of" };
	private String finalSentence = "";

	private void getListOfWordsTitlelize(String toTitlelize) {
		try {
			String[] wordslist = toTitlelize.split(" ");
			for (int i = 0; i < wordslist.length; i++) {
				words.add(titlelizeOneWord(wordslist[i]));
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	public String titlelizeOneWord(String toTitlelize) {

		if (toTitlelize.compareToIgnoreCase(exceptions[0]) == 0 || toTitlelize.compareToIgnoreCase(exceptions[1]) == 0 || toTitlelize.compareToIgnoreCase(exceptions[2]) == 0 ||
				 toTitlelize.compareToIgnoreCase(exceptions[3]) == 0 || toTitlelize.compareToIgnoreCase(exceptions[4]) == 0) {
			return toTitlelize;
		}

		return toTitlelize.substring(0, 1).toUpperCase() + toTitlelize.substring(1).toLowerCase();
	}

	@Override
	public String titlelize(String toTitlelize) {
		if (toTitlelize == "") {
			return "";
		}
		getListOfWordsTitlelize(toTitlelize);

		for (int i = 0; i < words.size(); i++) {
			if (i == words.size() - 1) {
				finalSentence += words.get(i);
			} else {
				finalSentence += words.get(i) + " ";
			}
		}

		return finalSentence;
	}

}
