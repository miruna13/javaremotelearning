package com.iquestgroup.remotelearning;

public class App 
{
    public static void main( String[] args ) throws Throwable
    {
        Tank tank = new Tank();
        Tank tank2 = new Tank();
        Tank tank3 = new Tank();
        Tank tank4 = new Tank();
        
        tank.empty();
        tank = new Tank();
        tank.fill();
        System.gc();
        // != gb 
        
        tank = new Tank();
        tank.fill();
        tank.empty();
        System.gc();
        // = gb for tank
        
        tank2.empty();
        System.gc();
        // = gb for line 13
        
        tank.fill(); // fill line 18
        System.gc();
        // != gb 
        
        tank3.empty();
        tank3.fill();
        System.gc();
        
        new Tank();
        tank3.fill();
        System.gc();
        
        tank4.fill();
        System.gc();
        //gb for line 37
        
    }
}
