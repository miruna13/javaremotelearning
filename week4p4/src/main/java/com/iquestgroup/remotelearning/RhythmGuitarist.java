package com.iquestgroup.remotelearning;

import Interfaces.MusicianImpl;

public class RhythmGuitarist implements MusicianImpl {
	
	private String guitaristType;
	private Guitar guitar;
	
	public RhythmGuitarist() {
		this.guitaristType = "Rhythm Guitarist";
	}
	
	@Override
	public void playInstrument() {
		System.out.println("This musician plays guitar");
		guitar.play();
	}

}
