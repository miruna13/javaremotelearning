package com.iquestgroup.remotelearning.devices;

public interface Device {
	void turnOn();
	void turnOff();
	void play();
	void showSongs();
}
