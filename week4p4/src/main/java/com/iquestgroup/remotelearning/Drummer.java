package com.iquestgroup.remotelearning;

import Interfaces.MusicianImpl;

public class Drummer implements MusicianImpl {
	
	private DrummSet drummset = new DrummSet();
	
	@Override
	public void playInstrument() {
		System.out.println("This musician plays drummSet");
		drummset.play();
	}

}
