package com.iquestgroup.remotelearning;

public class Domain {
	
	private String name;
	private String owner;
	private String hosts;
	
	public Domain(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
