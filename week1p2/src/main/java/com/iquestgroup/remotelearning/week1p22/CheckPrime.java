package com.iquestgroup.remotelearning.week1p22;

public class CheckPrime {
	
	private int value;
	private boolean isPrime;
	
	public CheckPrime(Number number) {
		this.value = number.getValue();
	}
	
	private boolean verifyIfNumberIsPrime() {
		for (int i = 2; i <= Math.sqrt(value); i++) {
			if (value % i == 0) {
				return false;
			}
		}
		return true;
	}
	
	public void printIfNumberIsPrime() {
		if(verifyIfNumberIsPrime()) {
			System.out.println("Number " + value + " is prime");
		} else {
			System.out.println("Number " + value + " is not prime");
		}
	}
	
}
