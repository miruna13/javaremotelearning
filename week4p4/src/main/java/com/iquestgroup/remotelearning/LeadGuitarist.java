package com.iquestgroup.remotelearning;

import Interfaces.MusicianImpl;

public class LeadGuitarist implements MusicianImpl{
	
	private String guitaristType;
	private Guitar guitar;
	
	public LeadGuitarist() {
		this.guitaristType = "Lead Guitarist";
	}
	
	@Override
	public void playInstrument() {
		System.out.println("This musician plays guitar");
		guitar.play();
	}


}
