package com.iquestgroup.remotelearning;


public class App 
{
    public static void main( String[] args )
    {
        Deck deck = new Deck();
        deck.printDeck();
        deck.shuffle();
        System.out.println("Shuffle");
        deck.printDeck();
    }
}
