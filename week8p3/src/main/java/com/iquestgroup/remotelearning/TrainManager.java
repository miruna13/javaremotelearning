package com.iquestgroup.remotelearning;

import java.util.HashSet;
import java.util.Set;

public class TrainManager {

	private Set<Train> trainsSet;

	public TrainManager() {
		trainsSet = new HashSet<Train>();
	}

	public void addTrain(Train objTrain) {
		if (checkDuplicate(objTrain)) {
			trainsSet.add(objTrain);
			System.out.println("Train stored");
		} else {
			System.out.println("Train already stored");
		}
	}

	public boolean checkDuplicate(Train objTrain) {
		for (Train auxTrain : trainsSet) {
			if (!auxTrain.equals(objTrain)) {
				return false;
			}
		}
		return true;
	}

	public Set<Train> getTrainsSet() {
		return trainsSet;
	}
	
}
