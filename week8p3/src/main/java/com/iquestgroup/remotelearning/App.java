package com.iquestgroup.remotelearning;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        TrainManager trainManager = new TrainManager();
        Train train1 = new Train(1, 1, 7);
        Train train2 = new Train(1, 1, 8);
        Train train3 = new Train(1, 1, 7);
        Train train4 = new Train(2, 1, 5);
        
        System.out.println(train1.equals(train2));
        System.out.println(train1.equals(train3));
        System.out.println(train1.equals(train4));
        
    }
}
