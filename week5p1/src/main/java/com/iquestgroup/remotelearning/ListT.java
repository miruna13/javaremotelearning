package com.iquestgroup.remotelearning;

public interface ListT<T> {

	void add(T element) throws CustomException;
	
	T get(int positon) throws CustomException;
	
	boolean contains(T element);
	
	boolean containsAll(ListT<T> foreignList) throws CustomException;
	
	int size();
}
