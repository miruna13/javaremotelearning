package com.iquestgroup.remotelearning;

public class Person extends Holder{
	
	private String nationality;
	private int age;
	private String description;
	
	public Person(String fullname, String nationality, int age) {
		super(fullname);
		this.nationality = nationality;
		this.age = age;
	}

	@Override
	String getBirthDate() {
		return null;
	}

	@Override
	String SelfDescribe() {
		if(nationality.equals("romana")) {
			description = "Numele meu este " + getFullname() + " si am " + age + " de ani";
		} else if (nationality.equals("english")) {
			description = "My name is " + getFullname() + " and I am " + age + " years old";
		}
		return description;
	}
	
}
