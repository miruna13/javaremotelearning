package com.iquestgroup.remotelearning.new1;

import java.util.List;

import com.iquestgroup.remotelearning.old.TaxiDriver;

public class DispatchOperator implements Human, Dispatch {
	
	private String name;
    private List<TaxiDriver> drivers;

    public DispatchOperator(String name, List<TaxiDriver> drivers) {
        this.name = name;
        this.drivers = drivers;
    }
    
    private TaxiDriver getBestAvailableTaxi(String location) {
        return drivers.get(0);
    }
	
	@Override
	public void dispatch(String location) {
		getBestAvailableTaxi(location).goToAddress(location);	
	}

	@Override
	public void eat() {
		System.out.println("Operator " + name + " eats");
	}

	@Override
	public void sleep() {
		System.out.println("Operator " + name + " sleeps");
	}

}
