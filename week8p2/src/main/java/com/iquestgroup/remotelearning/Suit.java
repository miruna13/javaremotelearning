package com.iquestgroup.remotelearning;

public enum Suit {
	CLUBS(1),
	DIAMONDS(2),
	HEARTS(3),
	SPADES(4);
	
	private int idType;

	Suit(int idType) {
		this.idType = idType;
	}

	public int getIdType() {
		return idType;
	}
	
	public String toString() {
        switch(this) {
        case CLUBS: return "Clubs";
        case DIAMONDS: return "Diamonds";
        case HEARTS: return "Hearts";
        case SPADES: return "Spades";
        default: return "ERROR: no valid suit";
        }
    }
	
}
