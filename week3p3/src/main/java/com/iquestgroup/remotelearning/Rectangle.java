package com.iquestgroup.remotelearning;

import java.util.ArrayList;

public class Rectangle extends Shape{
	
	private ArrayList<Line> rectangleLines = new ArrayList<Line>(4);
	
	public Rectangle(Line line1, Line line2, Line line3, Line line4) {
		rectangleLines.add(line1);
		rectangleLines.add(line2);
		rectangleLines.add(line3);
		rectangleLines.add(line4);
	}

	@Override
	void draw() {
		for(Line newLine : rectangleLines) {
			newLine.draw();
		}
		System.out.println("Rectangle draw");
	}

}
