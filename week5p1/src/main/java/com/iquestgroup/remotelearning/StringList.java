package com.iquestgroup.remotelearning;

import java.util.ArrayList;
import java.util.List;

public class StringList implements ListT<String> {

	private Integer[] stringList;
	ArrayList<String> history = new ArrayList<String>();

	@Override
	public void add(String element) throws CustomException {

		try {
			stringList[stringList.length] = Integer.parseInt(element);
		} catch (NullPointerException e) {
			if (element == null) {
				throw new CustomException("Null");
			}
		} catch (NumberFormatException e) {
			throw new CustomException("Invalid number.");
		}

	}

	@Override
	public String get(int positon) throws CustomException {
		history.add("get");
		try {
			return String.valueOf(stringList[positon]);
		} catch (IndexOutOfBoundsException e) {
			throw new CustomException("Index out of bounds.");
		}
	}

	@Override
	public boolean contains(String element) {
		for (int i = 0; i < stringList.length; i++) {
			if (stringList[i] == Integer.parseInt(element)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean containsAll(ListT<String> foreignList) throws CustomException  {
		int contor = 0;
		for (int i = 0; i < foreignList.size(); i++) {
			for (int j = 0; j < stringList.length; j++) {
				try {
					if (stringList[i] == Integer.parseInt(foreignList.get(i))) {
						contor++;
					}
				} catch (NumberFormatException e) {
					throw new CustomException("Invalid number.");
				}
			}
		}
		
		if(contor == foreignList.size()) {
			return true;
		}
		return false;
	}

	@Override
	public int size() {
		return stringList.length;
	}
	
	public void printHistory() {
		System.out.println(history.toString());
	}


}
