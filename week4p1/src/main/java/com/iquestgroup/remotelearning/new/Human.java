package com.iquestgroup.remotelearning.new1;

public interface Human {
	void eat();
	void sleep();
}
