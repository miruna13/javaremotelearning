package Interfaces;

public interface DeviceImpl {
	void turnOn();
	void turnOff();
	void play();
	void showSongs();
}
