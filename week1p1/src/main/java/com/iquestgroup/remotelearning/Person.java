package com.iquestgroup.remotelearning;

public class Person {

	private String firstName;
	private String lastName;
	private String dob;
	private String dod;

	public Person(String firstName, String lastName, String dob) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.dob = dob;
	}

	@Override
	public String toString() {
		return this.firstName + " " + this.lastName + "(" + this.dob + " " + this.dod +")" + "\n";
	}
	
	@Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!Person.class.isAssignableFrom(obj.getClass())) {
            return false;
        }

        final Person other =  (Person) obj;
        
        if ((this.firstName == null) ? (other.firstName != null) : !this.firstName.equals(other.firstName)) {
            return false;
        }
        if ((this.lastName == null) ? (other.lastName != null) : !this.lastName.equals(other.lastName)) {
            return false;
        }
        if ((this.dob == null) ? (other.dob != null) : !this.dob.equals(other.dob)) {
            return false;
        }
        if ((this.dod == null) ? (other.dod != null) : !this.dod.equals(other.dod)) {
            return false;
        }

        return true;
    }

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getDod() {
		return dod;
	}

	public void setDod(String dod) {
		this.dod = dod;
	}
}
