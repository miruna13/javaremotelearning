package com.iquestgroup.remotelearning;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class Translator {
	
	String phrase;
	String phraseInPigLatin;
	ArrayList<String> words = new ArrayList<String>();

	public Translator(String phrase) {
		this.phrase = phrase;
		phraseInPigLatin = "";
	}
	
	public void translatePhrase() {
		 getWords();
		 for(int i = 0; i < words.size(); i++) {
			 phraseInPigLatin += translateWord(words.get(i)) + " ";
		 }
	}
	private void getWords() {
		StringTokenizer st = new StringTokenizer(phrase," ");
		 while (st.hasMoreTokens()) {  
	         words.add(st.nextToken());
	     }
	}
	
	private String translateWord(String word) {
		return word.substring(1) + word.substring(0, 1) + "ay";
	}
	
	public void printLatinWord() {
		System.out.println(phraseInPigLatin);
	}
	
}
