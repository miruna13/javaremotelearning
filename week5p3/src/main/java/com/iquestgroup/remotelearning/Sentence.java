package com.iquestgroup.remotelearning;

import java.util.Random;

public class Sentence {
	
	private String[] article = {"the", "a", "one", "some", "any"};
	private String[]  noun = {"boy", "girl", "dog", "town", "car"};
	private String[]  verb = {"drove", "jumped", "ran", "walked", "skipped"};
	private String[]  preposition = {"to", "from", "over", "under", "on"};
	private String sentence;
	
	public Sentence() {
		sentence = "";
	}

	private String selectRandomWord(String[] list) {
		Random random = new Random();
		return list[random.nextInt(list.length)];
	}
	
	public void createSentence() {
		String randomArticle = selectRandomWord(article);
		sentence += randomArticle.substring(0, 1).toUpperCase() + randomArticle.substring(1) + " ";
		sentence += selectRandomWord(noun) + " ";
		sentence += selectRandomWord(verb) + " ";
		sentence += selectRandomWord(preposition) + " ";
		sentence += selectRandomWord(article) + " ";
		sentence += selectRandomWord(noun) + ".";
	}
	
	public String getSentence() {
		return sentence;
	}
	
}
