package com.iquestgroup.remotelearning;

public class Train {
	
	private int trainNumber;
	private String trainType;
	private int noOfwagons;
	private TrainType trainTypeEnum;
	
	
	public Train(int trainNumber, int trainTypeValue, int noOfwagons) {
		super();
		this.trainNumber = trainNumber;
		setNameOfTrainType(trainTypeValue);
		this.noOfwagons = noOfwagons;
	}
	
	private void setNameOfTrainType(int trainTypeValue) {
		for(TrainType trainTypeEnum : TrainType.values()) {
			if(trainTypeEnum.getValue() == trainTypeValue) {
				this.trainType = trainTypeEnum.name();
			}
		}
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Train train = (Train) obj;
        return trainNumber == train.trainNumber &&
        		trainType.equals(train.trainType) &&
        				noOfwagons == train.noOfwagons;
	}
	
//3)
//	@Override
//	public int hashCode() {
//		return trainNumber;
//	}
	
	@Override
	public int hashCode() {
		return 3;
	}
	
	@Override
	public String toString() {
		return trainNumber + " " + trainType + " " + noOfwagons;
	}

}
