package com.iquestgroup.remotelearning;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class TrainManager {
	
	private HashMap<Train, List<Integer>> trainHash;
	
	public TrainManager() {
		trainHash = new HashMap<Train, List<Integer>>();
	}
	
	private Train generateRandomTrain() {
		Random randTrainNumber = new Random();
		Random randTrainType = new Random();
		Random randTrainNoOfWagons = new Random();
		
		int trainNumber = Math.abs(randTrainNumber.nextInt()) % (200 + 1);
		int trainType = randTrainType.nextInt(3) + 1;
		int noOfWagons = Math.abs(randTrainNoOfWagons.nextInt()) % (300 + 1);
		
		return new Train(trainNumber, trainType, noOfWagons);
	}
	
	private ArrayList generatRandomListOfDays() {
		ArrayList<Integer> listOfDays = new ArrayList<Integer>();
		Random randDays = new Random();
		Random randNoOfElementsOfArray = new Random();
		
		int noOfDays = randNoOfElementsOfArray.nextInt(364) + 1;
		
		for (int i = 0; i < noOfDays; i++) {
			listOfDays.add(randDays.nextInt(364) + 1);
		}
		
		return listOfDays;
	}
	
	public void generateElementsForHashMap() {
		for (int i = 0; i < 10000; i++) {
			trainHash.put(generateRandomTrain(), generatRandomListOfDays());
		}
	}

	public HashMap<Train, List<Integer>> getTrainHash() {
		return trainHash;
	}
	
	public void print() {
		for (Train train: trainHash.keySet()){
            String key = train.toString();
            String value = trainHash.get(train).toString();  
            System.out.println("Train: " + key + " Days: " + value);  
}
	}
	
}
