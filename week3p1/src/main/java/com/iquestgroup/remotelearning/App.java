package com.iquestgroup.remotelearning;

public class App 
{
    public static void main( String[] args )
    {
        Person person1 = new Person("Ionescu Miruna Cristina", "romana", 21);
        System.out.println(person1.SelfDescribe());
        Person person2 = new Person("Popescu Ana Maria", "english", 25);
        System.out.println(person2.SelfDescribe());
    }
}
