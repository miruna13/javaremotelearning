package com.iquestgroup.remotelearning;


public class Holder {
	
	private String firstname;
	private String surname;
	private String fullname;
	
	public Holder(String firstname, String surname) {
		this.firstname = firstname;
		this.surname = surname;
		this.fullname = firstname + " " + surname;
	}
	
	public Holder(String fullname) {
		this.firstname = getNames(fullname)[0];
		this.surname = getNames(fullname)[1];
		this.fullname = fullname;
	}
	
	private String[] getNames(String fullname) {
		String[] splitName = fullname.split(" ");
		return splitName;
	}
	
	@Override
	public String toString() {
		return "FirstName: " + firstname + " Surname: " + surname + " Fullname: " + fullname;
	}
	
	
}
