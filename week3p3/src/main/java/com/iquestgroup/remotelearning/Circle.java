package com.iquestgroup.remotelearning;

public class Circle extends Shape{
	
	private final double PI = 3.14; 
	private int radius;
	
	public Circle(int radius) {
		super();
		this.radius = radius;
	}

	@Override
	void draw() {
		System.out.println("Circle draw");
	}

}
