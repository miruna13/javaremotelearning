package com.iquestgroup.remotelearning;

public interface Instrument {
	void play();
}
