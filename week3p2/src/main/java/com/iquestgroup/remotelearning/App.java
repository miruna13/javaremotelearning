package com.iquestgroup.remotelearning;

/**
 * Done
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        
    	Amphibian frog = new Frog();
    	
    	frog.run();
    	frog.eat();
    	frog.jump();
    	frog.speak();
    	
    }
}
