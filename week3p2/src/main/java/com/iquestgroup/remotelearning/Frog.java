package com.iquestgroup.remotelearning;

public class Frog extends Amphibian {
	
	public Frog() {
		System.out.println("Frog created");
	}
	
	@Override
	public void speak() {
		System.out.println("Frog speak");
	}

	@Override
	public void run() {
		System.out.println("Frog run");
	}

	@Override
	public void eat() {
		System.out.println("Frog eat");
	}

	@Override
	public void jump() {
		System.out.println("Frog jump");
	}
	
}
