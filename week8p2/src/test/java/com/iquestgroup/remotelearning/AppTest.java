package com.iquestgroup.remotelearning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class AppTest {
	Deck unshuffleDeck;
	Deck shuffleDeck;

	@Before
	public void setUp() {
		this.unshuffleDeck = new Deck();
		this.shuffleDeck = new Deck();
		shuffleDeck.shuffle();
	}

	@Test
	public void checkNoOfCards() {
		assertEquals(unshuffleDeck.getDeck().size(), 52);
	}

	@Test
	public void fillDeck() {
		int i = 0;
		for (Number number : Number.values()) {
			if (number.getValuOfCard() == 11)
				continue;
			for (Suit suit : Suit.values()) {
				assertEquals(unshuffleDeck.getDeck().get(i).getNumber(), number);
				assertEquals(unshuffleDeck.getDeck().get(i).getSuit(), suit);
				i++;
			}
		}
	}

	@Test
	public void checkDeckAfterShuffle() {
		assertTrue(!unshuffleDeck.getDeck().equals(shuffleDeck.getDeck()));
	}

}
