package com.iquestgroup.remotelearning;

import Interfaces.MusicianImpl;

public class BassGuitarist implements MusicianImpl {
	
	private String guitaristType;
	private Guitar guitar;
	
	public BassGuitarist() {
		this.guitaristType = "Bass Guitarist";
	}
	
	@Override
	public void playInstrument() {
		System.out.println("This musician plays guitar");
		guitar.play();
	}

}
