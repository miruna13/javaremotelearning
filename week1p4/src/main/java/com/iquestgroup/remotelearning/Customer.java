package com.iquestgroup.remotelearning;

public class Customer {
	
	private String name;
	private Reseller reseller = new Reseller();
	
	public void buy(Domain domain, Registry registry) {
		reseller.requestRegistrar(domain, registry);
		if(reseller.responseToCustomer()) {
			System.out.println(domain.toString() + " added!");
		} else {
			System.out.println("Domain's name " + domain.toString() +" already exists!");
		}
	}
	
}
