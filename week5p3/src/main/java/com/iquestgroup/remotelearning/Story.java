package com.iquestgroup.remotelearning;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Story {
	
	private List<Paragraph> shortStory;
	
	private int generateRandomNumberOfParagraphs () {
		Random random = new Random();
		return random.nextInt(19) + 1;
	}
	
	public Story() {
		int capacity = generateRandomNumberOfParagraphs();
		this.shortStory = new ArrayList<Paragraph>(capacity);
		for(int i = 0; i < capacity; i++) {
			shortStory.add(new Paragraph());
		}
	}
	
	public void tellStory() {
		for(Paragraph newParagraph : shortStory) {
			newParagraph.printParagraph();
		}
	}
	
}
