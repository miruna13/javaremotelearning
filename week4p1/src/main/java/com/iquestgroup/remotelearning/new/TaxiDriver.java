package com.iquestgroup.remotelearning.new1;

public class TaxiDriver implements Driver, Human {
	
	private String name;

    public TaxiDriver(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

	@Override
	public void eat() {
		System.out.println("Driver " + name + " eats");
	}

	@Override
	public void sleep() {
		System.out.println("Driver " + name + " sleeps");
	}

	@Override
	public int goToAddress(String address) {
		System.out.println("Driver " + name + " goes to " + address);
        return 2; // mock value
	}

	@Override
	public String getCurrentLocation() {
		return "mock location";
	}

}
