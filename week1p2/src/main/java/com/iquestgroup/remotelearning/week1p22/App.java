package com.iquestgroup.remotelearning.week1p22;

import java.util.ArrayList;
import java.util.Scanner;

public class App {
	public static void main(String[] args) {


		for (int i = 1; i <= Integer.parseInt(args[0]); i++) {
			Number number = new Number(i);
			CheckPrime checkPrime = new CheckPrime(number);
			checkPrime.printIfNumberIsPrime();
		}


	}
}
