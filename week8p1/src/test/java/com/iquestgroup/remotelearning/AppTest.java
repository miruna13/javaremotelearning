package com.iquestgroup.remotelearning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {

	Map mapForSort;
	Map mapForBinarySearch;
	
	List<String> countryNames;
	List<String> capitalNames;
	ArrayList<Country> countries;

	@Before
	public void setUp() {
		this.countries = new ArrayList<Country>();
	    this.countryNames = new ArrayList<String>();
	    this.capitalNames = new ArrayList<String>();
	    
		countries.add(new Country("Romania", "Bucuresti"));
		countries.add(new Country("Franta", "Paris"));
		countries.add(new Country("Olanda", "Amsterdam"));
		countries.add(new Country("Grecia", "Atena"));
		countries.add(new Country("Germania", "Berlin"));
		countries.add(new Country("Elvetia", "Berna"));
		countries.add(new Country("Ungaria", "Budapesta"));
		countries.add(new Country("Armenia", "Erevan"));
		this.mapForSort = new Map(countries);

		countryNames.add("Romania");
		countryNames.add("Franta");
		countryNames.add("Olanda");
		countryNames.add("Grecia");
		countryNames.add("Germania");
		countryNames.add("Elvetia");
		countryNames.add("Ungaria");
		countryNames.add("Armenia");
		
		capitalNames.add("Bucuresti");
		capitalNames.add("Paris");
		capitalNames.add("Amsterdam");
		capitalNames.add("Atena");
		capitalNames.add("Berlin");
		capitalNames.add("Berna");
		capitalNames.add("Budapesta");
		capitalNames.add("Erevan");

	}

	@Test
	public void checkSortName() {
		mapForSort.sort();
		Collections.sort(countryNames);

		for (int i = 0; i == mapForSort.getMap().size(); i++) {
			assertEquals(mapForSort.getMap().get(i).getName(), countryNames.get(i));
		}
	}
	
	@Test
	public void checkSortCapital() {
		mapForSort.sortCapital();
		Collections.sort(capitalNames);

		for (int i = 0; i == mapForSort.getMap().size(); i++) {
			assertEquals(mapForSort.getMap().get(i).getName(), capitalNames.get(i));
		}
	}
	
	@Test
	public void checkBinarySearchIfParisIsNotPresent() {
		ArrayList<Country> listForBinarySearch = new ArrayList<Country>();
		
		listForBinarySearch.add(new Country("Romania", "A"));
		listForBinarySearch.add(new Country("Olanda", "B"));
		listForBinarySearch.add(new Country("Grecia", "D"));
		listForBinarySearch.add(new Country("Germania", "E"));
		listForBinarySearch.add(new Country("Elvetia", "B"));
		listForBinarySearch.add(new Country("Ungaria", "C"));
		listForBinarySearch.add(new Country("Armenia", "F"));
		
		mapForBinarySearch = new Map(listForBinarySearch);
		assertTrue(mapForBinarySearch.findBinarySearch("Paris") == -1);
	}
	
	@Test
	public void checkBinarySearchIfParisIsMiddle() {
		ArrayList<Country> listForBinarySearch = new ArrayList<Country>();
		
		listForBinarySearch.add(new Country("Romania", "A"));
		listForBinarySearch.add(new Country("Germania", "W"));
		listForBinarySearch.add(new Country("Olanda", "C"));
		listForBinarySearch.add(new Country("Franta", "Paris"));
		listForBinarySearch.add(new Country("Grecia", "S"));
		listForBinarySearch.add(new Country("Elvetia", "B"));
		listForBinarySearch.add(new Country("Armenia", "T"));
		
		mapForBinarySearch = new Map(listForBinarySearch);
		assertTrue(mapForBinarySearch.findBinarySearch("Paris") == listForBinarySearch.size() / 2);
	}
	
	@Test
	public void checkBinarySearchIfParisIsLeft() {
		ArrayList<Country> listForBinarySearch = new ArrayList<Country>();
		
		listForBinarySearch.add(new Country("Romania", "R"));
		listForBinarySearch.add(new Country("Germania", "H"));
		listForBinarySearch.add(new Country("Olanda", "K"));
		listForBinarySearch.add(new Country("Franta", "Paris"));
		listForBinarySearch.add(new Country("Grecia", "W"));
		listForBinarySearch.add(new Country("Elvetia", "Y"));
		listForBinarySearch.add(new Country("Armenia", "Q"));
		
		mapForBinarySearch = new Map(listForBinarySearch);
		assertTrue(mapForBinarySearch.findBinarySearch("Paris") < listForBinarySearch.size() / 2);
	}
	
	@Test
	public void checkBinarySearchIfParisIsRight() {
		ArrayList<Country> listForBinarySearch = new ArrayList<Country>();
		
		listForBinarySearch.add(new Country("Romania", "A"));
		listForBinarySearch.add(new Country("Olanda", "H"));
		listForBinarySearch.add(new Country("Grecia", "C"));
		listForBinarySearch.add(new Country("Franta", "Paris"));
		listForBinarySearch.add(new Country("Germania", "G"));
		listForBinarySearch.add(new Country("Elvetia", "B"));
		listForBinarySearch.add(new Country("Armenia", "D"));
		
		mapForBinarySearch = new Map(listForBinarySearch);
		assertTrue(mapForBinarySearch.findBinarySearch("Paris") > listForBinarySearch.size() / 2);
	}
	
}
