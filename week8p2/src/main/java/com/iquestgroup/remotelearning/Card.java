package com.iquestgroup.remotelearning;

public class Card {
	
	private Number number;
	private Suit suit;
	
	
	public Card(Number number, Suit suit) {
		this.number = number;
		this.suit = suit;
	}


	public Number getNumber() {
		return number;
	}


	public void setNumber(Number number) {
		this.number = number;
	}


	public Suit getSuit() {
		return suit;
	}


	public void setSuit(Suit suit) {
		this.suit = suit;
	}
	
	@Override
	public String toString() {
		return number.toString() + " of " + suit.toString();
	}
	
}
