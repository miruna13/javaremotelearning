package com.iquestgroup.remotelearning;

public interface Musician {
	void playInstrument();
}
