package com.iquestgroup.remotelearning;

import java.util.ArrayList;
import java.util.List;

public class Registry {
	
	private List<Domain> domainsList = new ArrayList<Domain>();
	
	public void add(Domain newDomain) {
		domainsList.add(newDomain);
	}

	public List<Domain> getDomainsList() {
		return domainsList;
	}
	
}
