package com.iquestgroup.remotelearning.new1;

public interface Driver {
	int goToAddress(String address);
	String getCurrentLocation();
}
