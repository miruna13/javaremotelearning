package com.iquestgroup.remotelearning;

abstract class Shape {
	
	abstract void draw();
	
}
