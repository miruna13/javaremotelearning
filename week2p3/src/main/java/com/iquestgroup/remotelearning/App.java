package com.iquestgroup.remotelearning;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        
    	Holder holder = new Holder("Ionescu", "Miruna");
    	System.out.println(holder.toString());
    	
    	Holder holder2 = new Holder("Ionescu Miruna");
    	System.out.println(holder2.toString());
    	
    	Holder holder3 = new Holder("Ionescu Miruna Cristina");
    	System.out.println(holder3.toString());
    	
    }
}
