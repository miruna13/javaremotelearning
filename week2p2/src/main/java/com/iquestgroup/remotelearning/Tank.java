package com.iquestgroup.remotelearning;

public class Tank {
	
	private boolean filled;
	
	public Tank() {
		filled = false;
	}
	
	public void fill() {
		System.out.println("Fill tank " + this);
		filled = true;
	}
	
	public void empty() {
		System.out.println("Empty tank " + this);
		filled = false;
	}
	
	@Override
	protected void finalize() throws Throwable {
		if(filled == true) {
			System.out.println("Tank is filled, it must be empty to be cleaned " + this);
		} else {
			System.out.println("Tank is cleaned " + this);
		}
	}
	
	public boolean getStatus() {
		return filled;
	}
	
}
