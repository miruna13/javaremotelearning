package com.iquestgroup.remotelearning;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class ListOfPersons {

	private List<Person> list = new ArrayList<Person>();

	public ListOfPersons(String fileName) {
		ReadFromFile readFromFile = new ReadFromFile();
		try {
			String[] stringFromFile = readFromFile.readLinesFromTextFile(fileName);
			for (int i = 0; i < stringFromFile.length; i++) {
				String[] stringPerson = stringFromFile[i].split(",");
				Person newPerson = new Person(stringPerson[0], stringPerson[1], stringPerson[2]);
				if (stringPerson.length == 4) {
					newPerson.setDod(stringPerson[3]);
				} else {
					newPerson.setDod("");
				}

				if (!checkDuplicates(newPerson)) {
					list.add(newPerson);
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean checkDuplicates(Person newPerson) {

		for (Person auxPerson : list) {
			if (auxPerson.equals(newPerson)) {
				return true;
			}

		}
		return false;
	}

	public void printListOfPeople() {
		System.out.println(list.toString());
	}
	
}
