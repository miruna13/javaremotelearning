package com.iquestgroup.remotelearning.new1;

import java.util.ArrayList;
import java.util.List;

import com.iquestgroup.remotelearning.old.DispatchOperator;
import com.iquestgroup.remotelearning.old.TaxiDriver;

public class Main {

	public static void main(String[] args) {
		List<TaxiDriver> drivers = new ArrayList<TaxiDriver>();
        drivers.add(new TaxiDriver("Vlad"));
        DispatchOperator operator = new DispatchOperator("Dan", drivers);

        operator.dispatch("Calea Motilor nr. 6");
	}

}
