package com.iquestgroup.remotelearning.week1p22;

public class Number {

	private int value;

	public Number(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

}
