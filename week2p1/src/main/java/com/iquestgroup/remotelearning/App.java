package com.iquestgroup.remotelearning;

/**
 * 1: Base Class static variable;
 * 2: Base Class static block;
 * 3: Super Class static variable;
 * 4: Super Class static block;
 * 5: Base class variable;
 * 6: Base class field;
 * 7: Base class constructor because SuperClass automatically call super()
 * 8: Super class variable
 * 9: Super class field
 * 10: Super class constructor 
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	SuperClass superClass = new SuperClass();
    }
}
