package com.iquestgroup.remotelearning;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	Internet internet = new ProxyFactory(); 
        try
        { 
            internet.connectTo("siteBun"); 
            internet.connectTo("abc.com");
        } 
        catch (Exception e) 
        { 
            System.out.println(e.getMessage()); 
        } 
        
        ProxyManager proxyManager = new ProxyManager();
        proxyManager.a(internet, "connectTo");
    }
}
