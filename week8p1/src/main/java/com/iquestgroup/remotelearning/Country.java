package com.iquestgroup.remotelearning;

import java.awt.SecondaryLoop;

public class Country implements Comparable<Country>{
	
	private String name;
	private String capital;
	
	
	public Country(String name, String capital) {
		this.name = name;
		this.capital = capital;
	}


	@Override
	public int compareTo(Country secoundCountry) {
		return this.name.compareTo(secoundCountry.getName());
	}
	
	@Override
	public String toString() {
		return name + " " + capital;
	}


	public String getName() {
		return name;
	}


	public String getCapital() {
		return capital;
	}
	
	
	
}
