package com.iquestgroup.remotelearning;

import java.util.List;
import java.util.ArrayList;

public class Canvas extends Shape {

	private List<Shape> shapes;

	public Canvas() {
		shapes = new ArrayList<Shape>();
	}

	public void addCanvas(Canvas newCanvas) {
		shapes.add(newCanvas);
	}

	public void addShape(Shape shape) {
		shapes.add(shape);
	}

	@Override
	void draw() {
		System.out.println("Canvas: ");
		for (Shape principalCanvasShapes : shapes) {
			principalCanvasShapes.draw();
		}
		System.out.println();
	}

	public List<Shape> getShapes() {
		return shapes;
	}

}
