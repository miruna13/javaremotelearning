package com.iquestgroup.remotelearning;

public class SuperClass extends BaseClass{
	
	public int var = function(1);
	public static int staticVar = function(10);
	
	public SuperClass() {
		System.out.println("SuperClass Constructor");
	}
	
	{
		System.out.println("SuperClass Field");
	}
	
	static {
		System.out.println("SuperClass Static block");
	}
	
	private static int function(int number) {
		System.out.println("SuperClass Number: " + number);
		return number;
	}

}
