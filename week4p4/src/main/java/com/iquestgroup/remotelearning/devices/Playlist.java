package com.iquestgroup.remotelearning.devices;

import java.util.List;

public class Playlist {
	
	private List<Song> playlist;
	
	public Playlist(List<Song> playlist) {
		this.playlist = playlist;
	}
	
	public void addSongs(String name) {
		Song newSong = new Song(name);
		playlist.add(newSong);
	}
	
	public void showPlaylist() {
		for(int i = 0; i < playlist.size(); i++) {
			System.out.println(playlist.get(i).getName());
		}
	}
	
	public void playPlaylist() {
		for(int i = 0; i < playlist.size(); i++) {
			System.out.println(playlist.get(i).getName());
		}
	}

	public List<Song> getPlaylist() {
		return playlist;
	}
	
}
