package com.iquestgroup.remotelearning;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Date date = new Date(1, 2, 2020);
        OutputDate outputDate = new OutputDate(date);
        outputDate.printDate();
    }
}
