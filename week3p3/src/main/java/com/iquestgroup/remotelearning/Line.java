package com.iquestgroup.remotelearning;

import java.util.ArrayList;

public class Line extends Shape{
	
	private ArrayList<Point> linePoints = new ArrayList<Point>(2);
	
	public Line(Point pointStart, Point pointEnd) {
			linePoints.add(pointStart);
			linePoints.add(pointEnd);
	}

	@Override
	public void draw() {
		for(Point newPoint : linePoints) {
			newPoint.draw();
		}
		System.out.println("Line draw");
	}
	
}
