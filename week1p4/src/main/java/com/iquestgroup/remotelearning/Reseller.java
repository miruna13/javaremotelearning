package com.iquestgroup.remotelearning;

public class Reseller {
	
	private boolean response;
	private Registrar registrar = new Registrar();
	
	public void requestRegistrar( Domain newDomain, Registry registry) {
		if(registrar.check(registry, newDomain)) {
			response = true;
		} else {
			response = false;
		}
	}
	
	public boolean responseToCustomer() {
		return response;
	}
	
}
