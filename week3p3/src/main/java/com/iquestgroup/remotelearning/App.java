package com.iquestgroup.remotelearning;

/**
 * Done
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	
    	int radius = 30;
    	Line line1 = new Line(new Point(1,2), new Point(3,4));
    	Line line2 = new Line(new Point(1,2), new Point(3,4));
    	Line line3 = new Line(new Point(1,2), new Point(3,4));
    	Line line4 = new Line(new Point(1,2), new Point(3,4));
    	
    	Circle circle = new Circle(radius);
    	Rectangle rectangle = new Rectangle(line1, line2, line3, line4);
    	
        Canvas canvas = new Canvas();
        Canvas canvas2 = new Canvas();
        
        canvas.addShape(circle);
        canvas.addShape(rectangle);
        canvas.draw();
        
        canvas2.addShape(circle);
        canvas2.addShape(circle);
        canvas2.draw();
        
        canvas.addCanvas(canvas2);
        canvas.draw();
        
    }
}
