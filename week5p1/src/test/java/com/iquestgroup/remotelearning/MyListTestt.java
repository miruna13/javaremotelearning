package com.iquestgroup.remotelearning;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class MyListTestt {
    
    private List<String> list;
    
    private Class<CustomException> exceptionType = CustomException.class;
    
    private String[] initData = {"12", "23", "34", "45"};
    
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @SuppressWarnings("unchecked")
	@Before
    public void setUp() {
    	this.list = (List)new StringList();
    }
    
    @Test
    public void testAddValuesToTheList() {
        initData();
    	assertEquals(initData.length, list.size());
        for (String data : initData) {
        	assertTrue(list.contains(data));
        }
    }

    private void initData() {
        for (String numberAsString : initData) {
            list.add(numberAsString);
        }
        System.out.println(list);
    }
    
    @Test
    public void testAddNonIntegerValue() {
        exception.expect(exceptionType);
        exception.expectMessage("Invalid number.");
        list.add("Hey, I'm not an integer.");
    }
    
    @Test
    public void testAddNonNullValue() {
        exception.expect(exceptionType);
        exception.expectMessage("Null");
        list.add(null);
    }
    
    @Test
    public void testIndexOutOfBounds() {
        initData();
        exception.expect(exceptionType);
        exception.expectMessage("Index out of bounds.");
        list.get(initData.length);
    }
}
