package com.iquestgroup.remotelearning.devices;

import java.util.List;

import Interfaces.DeviceImpl;

public class Ipod implements DeviceImpl {
	
	private Playlist playlist;
	
	public Ipod(List<Song> songs) {
		this.playlist = new Playlist(songs);
	}
	
	@Override
	public void turnOn() {
		System.out.println("Ipod turn on");
	}

	@Override
	public void turnOff() {
		System.out.println("Ipod turn off");
	}

	@Override
	public void play() {
		System.out.println("Ipod's playlist plays");
		playlist.playPlaylist();
	}

	@Override
	public void showSongs() {
		System.out.println("Ipod's playlist");
		if (playlist.getPlaylist().size() != 0) {
			playlist.showPlaylist();
		}else {
			System.out.println("No more songs");
		}
	}

}
